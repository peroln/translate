<?php

namespace api\modules\v3\controllers;

//use common\models\search\BookSearch;
use common\components\UploadFile;
use common\models\search\BookSearch;
use Gufy\PdfToHtml\Config;
use Gufy\PdfToHtml\Html;
use Gufy\PdfToHtml\Pdf;
use Yii;
use common\models\Book;


use yii\base\Exception;
use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
//        $behaviors['authenticator'] = [
//            'class' => CompositeAuth::className(),
//            'authMethods' => [
//                QueryParamAuth::className(),
//            ],
//            'only' => [
//                'all',
//                'one',
//            ],
//        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'convert'
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                        'convert'
                    ],
                    'allow' => true,
                    'roles' => ['?'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'convert' => ['POST']
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Book models.
     * book/all
     * get
     *
     * discipline_id
     *
     * @return mixed
     */

    public function actionAll()
    {
        $path = dirname(Yii::getAlias('@app')).'/vendor/gufy/pdftohtml-php/output/5840f5e133000/35840f5e-1.html';
        $fp = fopen($path, 'r+');
        $i = 0;
        while(!feof($fp)){
            $text = fgets($fp, 999);
            $text = preg_replace_callback('|([ЁЙЦУКЕНГШЩЗХФЫВАПРОЛДЖЭЯЧСМИТБЮёйцукенгшщзхъфывапролджэячсмитьбю]+)|i', function($matches)
   {
       global $i;
       $i++;
       $matches[1] = '<span id='. $i .'>'.$matches[1].'</span>';
       return $matches[1];

   }, $text);
/*           if (preg_match('|<p[^>].*?>\s*([А-Яёйцукенгшщзхъфывапролджэячсмитьбю\s]+)</p>|i', $text, $arr)){*/
////                $arr[1] = '<div id='.$i.'>'.$arr[1].'</div>';
//                $paragrapf[] = $arr[1];

//            }
echo $text;
         $i++;
        }

        fclose($fp);
        return $text;
//return $paragrapf;
        $model = new BookSearch();


        $result = $model->searchAll(Yii::$app->request->get('discipline_id'));

        return $result ? $model->all_fields($result) : $model->getErrors();
    }


    /**
     * Displays a single Book model.
     * book/one
     * get
     * @property integer $id+
     *
     * @return mixed
     */

    public function actionOne()
    {
        $model = $this->findModel(Yii::$app->request->get('id'));
        return [
            'book' => $model->one_fields(),
            'label' => $model->attributeLabels()
        ];
    }

    public function actionConvert(){
if($_FILES['file']){
    $file = new UploadFile();
    $myFile = $file->upload('file', 1, 'PDF');

// change pdftohtml bin location
    Config::set('pdftohtml.bin',  dirname(Yii::getAlias('@app')).'/vendor/gufy/poppler-0.49/bin/pdftohtml.exe');

// change pdfinfo bin location
    Config::set('pdfinfo.bin', dirname(Yii::getAlias('@app')). '/vendor/gufy/poppler-0.49/bin/pdfinfo.exe');

        $pdf = new Pdf($myFile['path']);

       return['currentPage' => $pdf->html(), 'countPages' => $pdf->getPages() ];
}
    return 'Нет файла';

    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            if ($model->deleted == Book::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
