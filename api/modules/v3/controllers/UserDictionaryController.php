<?php

namespace api\modules\v3\controllers;

use Yii;
use common\models\english\UserDictionary;
use common\models\english\search\UserDictionarySearch;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * UserDictionaryController implements the CRUD actions for UserDictionary model.
 */
class UserDictionaryController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'one',
                        'all',
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],
            ],
        ];
        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['post'],
            ],
        ];
        return $behaviors;
    }

    /**
     * Lists all UserDictionary models.
     * user-dictionary/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new UserDictionarySearch();
        $model->user_id = Yii::$app->user->id;
        $arr = [];
        foreach (range('a', 'z') as $letter) {
            $model->en_letter = $letter;
            $result = $model->searchAll();
            if ($result['models']) {
                $arr[$letter] = $model->all_fields($result);
            }
        }
        return $arr ? ['dictionary' => $arr ]
            : [ 'dictionary' => false ];
    }

    /**
     * Displays a single UserDictionary model.
     * UserDictionary/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new UserDictionary model.
     * user-dictionary/create
     * post
     *
     * @property integer $translate_id+
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserDictionary();
        $model->user_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->saveWithCheck() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }


    /**
     * Deletes an existing UserDictionary model.
     * user-dictionary/delete
     * delete
     *
     * @property integer $id +

     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        $model = new UserDictionary;
        $model->user_id = Yii::$app->user->id;
        $model->translate_id = Yii::$app->request->post('translate_id');
        $result = $model->findModel();
        if($result){
            if ($result->remove() && !$result->getErrors()) {
                return true;
            }
            return $model->getErrors();
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the UserDictionary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserDictionary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserDictionary::findOne($id)) !== null) {
            if ($model->deleted == UserDictionary::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
