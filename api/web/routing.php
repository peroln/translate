<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Routing</title>
</head>
<body>

</body>
</html>

<p> URL: http://audio.project.vt-host.co.ua/backend/api/web/{module}/{controller}/{action}?access-token={token}</p>

<hr>
<h1>module: v1</h1>

<h2>controller: site</h2>
<p>action: auth method: GET params: (with authenticator access-token)</p>
<p>action: login method: POST params: 'phone', 'password'</p>
<p>action: request-password-reset method: POST params: 'email'</p>
<p>action: reset-password method: POST params: 'token', 'password'</p>

<hr>

<h2>controller: document</h2>

<p>action: index method: GET params: "page", "size", 'doctor_id', 'document_template_id', 'patient_id', 'name', 'verified', 'status', </p>
<p>action: create method: POST params: 'doctor_id', "document_template_id", 'patient_id', 'name'</p>
<p>action: update method: PUT params: "id", 'doctor_id', 'patient_id', 'name' </p>
<p>action: delete method: DELETE params: "id" </p>
<p>action: view method: GET params: "id" </p>

<hr>

<h2>controller: field</h2>

<p>action: index method: GET params: "page", "size", 'document_id', 'field_template_id', 'order', 'text', 'status', </p>
<p>action: create method: POST params: "document_id", "field_template_id", 'order', 'text' + audio</p>
<p>action: update method: PUT params: "id", "document_id", "field_template_id", 'order', 'text' + audio</p>
<p>action: delete method: DELETE params: "id" </p>
<p>action: view method: GET params: "id" </p>
<p>action: all method: GET params: "document_id", 'name' (идет поиск по LIKE) </p>

<p>action: upload-audio method: POST params: "id", 'symbol' + file </p>
<p>action: remove-audio method: DELETE params: "id" </p>

<hr>

<h2>controller: document-template</h2>

<p>action: all method: GET params: 'name' (идет поиск по LIKE) </p>

<hr>

<h2>controller: field-template</h2>

<p>action: all method: GET params: "document_template_id", 'name' (идет поиск по LIKE) </p>

<hr>