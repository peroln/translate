<?php

namespace common\models\english;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "eng_dictionary".
 *
 * @property integer $id
 * @property string $value
 * @property string $explanation
 * @property integer $intensity
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property User $updatedBy
 * @property User $createdBy
 * @property Translate[] $translates
 */
class EngDictionary extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eng_dictionary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'intensity', 'created_by', 'created_at'], 'required'],
            [['intensity', 'created_by', 'created_at', 'updated_by', 'updated_at', 'deleted'], 'integer'],
            [['value', 'explanation'], 'string', 'max' => 255],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'explanation' => 'Explanation',
            'intensity' => 'Intensity',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted' => 'Deleted',
        ];
    }
    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\Book' => [
                    'id',
                    'value',
                    'explanation',
                    'intensity',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                ],
            ]
        );
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslates()
    {
        return $this->hasMany(Translate::className(), ['english_id' => 'id']);
    }
}
