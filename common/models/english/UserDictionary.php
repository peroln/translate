<?php

namespace common\models\english;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use common\models\User;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "user_dictionary".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $translate_id
 * @property integer $created_at
 * @property integer $deleted
 *
 * @property Translate $translate
 * @property User $user
 */
class UserDictionary extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;
    public $english_id;
    public $en_letter;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_dictionary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'translate_id', 'created_at'], 'required'],
            [['user_id', 'translate_id', 'created_at', 'deleted'], 'integer'],
            [['en_letter'], 'safe'],
            [['english_id'], 'integer'],
            [['translate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Translate::className(), 'targetAttribute' => ['translate_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'translate_id' => 'Translate ID',
            'created_at' => 'Created At',
            'deleted' => 'Deleted',
        ];
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'english_id' => $this->translate->english_id,
            'translate_id' => $this->translate_id,
            'translate' => $this->translate->translate,
            'user_id' => $this->user_id,
            'user' => $this->user->first_name . " " . $this->user->last_name,
            'created_at' => $this->created_at,
        ];
    }

    public function all_fields($result)
    {
        $result = ArrayHelper::ToArray(
            $result['models'],
            [
                'common\models\english\UserDictionary' => [
                    'id',
                    'translate_id',
                    'translate' => function ($model) {
                        return $model->translate->translate;
                    },
                    'value' => function ($model) {
                        return $model->translate->english->value;
                    },
                    'english_id' => function ($model) {
                        return $model->translate->english_id;
                    },
                ],
            ]
        );
        return $result;

    }

    public function findModel(){
        return self::findOne(['user_id' =>$this->user_id, 'translate_id'=>$this->translate_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslate()
    {
        return $this->hasOne(Translate::className(), ['id' => 'translate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnglish()
    {
        return $this->hasMany(EngDictionary::className(), ['id' => 'english_id'])
            ->viaTable('translate', ['id' => 'translate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
