<?php

namespace common\models\english\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\english\Translate;

/**
 * TranslateSearch represents the model behind the search form about `common\models\english\Translate`.
 */
class TranslateSearch extends Translate
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'english_id', 'created_by', 'created_at', 'updated_by', 'updated_at', 'deleted', 'page', 'page_size'], 'integer'],
            [['translate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Translate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }else{
            $dataProvider->pagination = false;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'english_id' => $this->english_id,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted' => $this->deleted,
        ]);

        if($this->created_at){
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(`user_dictionary`.created_at))',  $this->created_at,  date('d-m-Y', time())]);
        }

        $query->andFilterWhere(['like', 'translate', $this->translate]);

        return $dataProvider;
    }
}
