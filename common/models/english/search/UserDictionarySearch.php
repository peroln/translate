<?php

namespace common\models\english\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\english\UserDictionary;

/**
 * UserDictionarySearch represents the model behind the search form about `common\models\english\UserDictionary`.
 */
class UserDictionarySearch extends UserDictionary
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'translate_id', 'created_at', 'deleted',  'page', 'page_size','en_letter'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = UserDictionary::find();

        $query->joinWith('english');

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }else{
            $dataProvider->pagination = false;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_dictionary.user_id' => $this->user_id,
            'user_dictionary.translate_id' => $this->translate_id,
            'user_dictionary.created_at' => $this->created_at,
            'user_dictionary.deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'eng_dictionary.value', $this->en_letter."%", false]);

        if($this->created_at){
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(`user_dictionary`.created_at))',  $this->created_at,  date('d-m-Y', time())]);
        }

        return $dataProvider;
    }
}
