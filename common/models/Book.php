<?php

namespace common\models;


use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Gufy\PdfToHtml\Html;
use Yii;
use common\models\search\BookSearch;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

use Gufy\PdfToHtml\Config;
use Gufy\PdfToHtml\Pdf;
//use vendor\gufy\pdftohtml-php\src\Gufy;

/**
 * This is the model class for table "book".
 *
 * @property integer $id
 * @property string $name
 * @property string $author
 * @property integer $class
 * @property integer $edition
 * @property integer $year
 * @property string $notes
 * @property string $paid_content
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property User $creator
 * @property User $updater
 * @property Section[] $sections
 * @property BookDiscipline[] $bookDisciplines
 * @property  $photoPath
 *
 */
class Book extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    const NOT_PAID_CONTENT = 0;
    const PAID_CONTENT = 1;

    public $error;
    public $photo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'author', 'year', 'edition', 'class', 'created_by', 'created_at',], 'required'],
            [['year', 'deleted', 'paid_content', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['notes','photo'], 'string'],
            [['name', 'author', 'edition',], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            ['paid_content', 'default', 'value' => self::NOT_PAID_CONTENT],
            ['paid_content', 'in', 'range' => [self::NOT_PAID_CONTENT, self::PAID_CONTENT]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/book", "ID"),
            'name' => Yii::t("msg/book", 'Name'),
            'author' => Yii::t("msg/book", 'Author'),
            'class' => Yii::t("msg/book", 'Class'),
            'year' => Yii::t("msg/book", 'Year'),
            'edition' => Yii::t("msg/book", 'Edition'),
            'notes' => Yii::t("msg/book", 'Notes'),
            'paid_content' => Yii::t("msg/book", 'Notes'),
            'created_by' => Yii::t("msg/book", 'Create by'),
            'updated_by' => Yii::t("msg/book", 'Update by'),
            'created_at' => Yii::t("msg/book", 'Date create'),
            'updated_at' => Yii::t("msg/book", 'Date update'),
            'deleted' => Yii::t("msg/book", 'Deleted'),
        ];
    }

    public function getPhotoPath()
    {
        if ($this->photo) {
            return Yii::$app->request->getHostInfo() . "/photo/books" . $this->id . "/" . $this->photo;
        } else {
            return Yii::$app->request->getHostInfo() . "/photo/books/empty_book.jpg";
        }
    }


    public function one_fields()
    {
        return [
            'id' =>$this->id,
            'image' =>$this->photoPath,
            'name'=>$this->name,
            'author'=>$this->author,
            'class'=>$this->class,
            'year'=>$this->year,
            'edition'=>$this->edition,
            'notes'=>$this->notes,
            'paid_content'=>$this->paid_content,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name. " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
            'updater' => $this->updater->first_name. " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }

    /**
     * search all free models
     * @param $request
     * @return array
     */

    public function searchFree($request)
    {
        $searchModel = new BookSearch();
        $searchModel->deleted = self::NOT_DELETED;
        if (!$searchModel->load(['BookSearch' => $request]) || !$searchModel->validate()) {
            return false;
        }
        $dataProvider = $searchModel->free();
        $models = $dataProvider->getModels();
        return $models;
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\Book' => [
                    'id',
                    'image' =>function($model){
                        return $model->photoPath;
},
                    'name',
                    'class',
                    'author',
                    'year',
                    'edition',
                    'notes',
                    'paid_content',
                ],
            ]
        );
        return $result;
    }


    public function remove()
    {
        $this->deleted = self::DELETED;
        if ($this->save()) {
            if ($this->bookDisciplines) {
                //удаляем записи принадлежности книг дисциплинам
                foreach ($this->bookDisciplines as $one) {
                    $book_discipline = BookDiscipline::findOne($one->id);
                    if ($book_discipline->remove() && $book_discipline->getErrors())
                        $this->addError('deleted', $book_discipline->getErrors());
                    return $this;
                }
            }
        }
        return $this;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Section::className(), ['book_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookDisciplines()
    {
        return $this->hasMany(BookDiscipline::className(), ['book_id' => 'id'])->andOnCondition(['book_discipline.deleted' => self::NOT_DELETED]);
    }

}
