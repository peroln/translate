<?php

namespace common\models;


use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\sqlite\Schema;

//use yii\rbac\Role;
//use yii\rbac\Role;
use yii\web\IdentityInterface;

use yii\web\UploadedFile;

use common\models\search\UserSearch;
use yii\helpers\ArrayHelper;

/*** User model
 *
 * @property string $password write-only password
 *
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $role_id
 * @property integer $confirm
 * @property integer $balance
 * @property string $email
 * @property string $phone
 * @property string $first_name
 * @property string $second_name
 * @property string $last_name
 * @property string $photo
 * @property string $auth_key
 * @property integer $school_id
 * @property integer $sity_id
 * @property string $password_hash
 * @property integer $push_status
 * @property integer $updated_at
 * @property integer $created_at
 * @property integer $deleted
 *
 * @property  $image_file
 *
 * @property  $photoPath
 * @property  $photoDir
 *
 * @property Checkup[] $checkups
 * @property Control[] $controls
 * @property Discipline[] $disciplines
 * @property DoneTask[] $doneTasks
 * @property MyClass[] $myClass
 * @property MyClass[] $myClasses
 * @property MyDiscipline[] $myDisciplines
 * @property MyPupil[] $myPupils
 * @property MyPupil[] $myTeachers
 * @property Task[] $tasks
 * @property Transfer[] $transfers
 * @property Role $role
 * @property School $school
 * @property Sity $sity
 * @property Subscription $subscription
 */
class User extends ActiveRecord implements IdentityInterface
{
    use soft;
    use findRecords;
    use errors;


    const NOT_DELETED = 0;
    const DELETED = 1;

    public $password;

    public $error;
    public $image_file;
    public $extension;
    public $sity_name;
    public $region_name;
    public $my_classes;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'phone', 'first_name', 'last_name', 'created_at'], 'required', 'except' => ['change_pass']],

            [['password'], 'required', 'on' => 'signup'],

            [['auth_key', 'password_hash',], 'required', 'on' => 'save'],

            [['password_hash', 'password'], 'required', 'on' => 'change_pass'],

            ['phone', 'trim'],
            //TODO require
//            ['phone', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This phone has already been taken.'],
//            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['email', 'trim'],
            ['email', 'email'],
            ['password', 'string', 'min' => 6],
            [['role_id', 'confirm', 'balance', 'school_id', 'sity_id', 'updated_at', 'created_at', 'push_status', 'deleted'], 'integer'],
            [['email', 'second_name', 'photo', 'password_hash'], 'string', 'max' => 255],
            [['first_name', 'last_name'], 'string', 'max' => 55],
            [['auth_key'], 'string', 'max' => 32],
            [['phone'], 'string', 'min' => 10,'max' => 10],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
            [['school_id'], 'exist', 'skipOnError' => true, 'targetClass' => School::className(), 'targetAttribute' => ['school_id' => 'id']],
            [['sity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sity::className(), 'targetAttribute' => ['sity_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],

            [['image_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, bmp, jpeg'],
            [['image_file'], 'safe'],

            [['sity_name', 'region_name', 'my_classes','extension'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {

        return [
            'id' => Yii::t("msg/user", "ID"),
            'role_id' => Yii::t("msg/user", "Role ID"),
            'role' => Yii::t("msg/user", "Role"),
            'confirm' => Yii::t("msg/user", "Confirm"),
            'balance' => Yii::t("msg/user", "Balance"),
            'phone' => Yii::t('msg/user', "Phone"),
            'password' => Yii::t('msg/user', "Password"),
            'email' => Yii::t('msg/user', "Email"),
            'first_name' => Yii::t("msg/user", "First Name"),
            'second_name' => Yii::t("msg/user", "Second Name"),
            'last_name' => Yii::t("msg/user", "Last Name"),
            'photo' => Yii::t("msg/user", "Photo"),
            'school' => Yii::t("msg/user", "School"),
            'sity' => Yii::t("msg/user", "City"),
            'class' => Yii::t("msg/user", "Class"),
            'discipline' => Yii::t("msg/user", "Discipline"),
            'pupil' => Yii::t("msg/user", "Pupil"),
            'teacher' => Yii::t("msg/user", "Teacher"),
            'created_at' => Yii::t("msg/user", 'Date create'),
            'updated_at' => Yii::t("msg/user", 'Date update'),
            'my_classes' => Yii::t("msg/user", 'My Classes'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getPhotoPath()
    {
        if ($this->photo) {
            return Yii::$app->request->getHostInfo() . "/photo/users/" . $this->id . "/" . $this->photo;
        } else {
            return Yii::$app->request->getHostInfo() . "/photo/users/empty.jpg";
        }
    }

    public function getPhotoDir()
    {
        return dirname(Yii::getAlias('@app')) . '/photo/users/' . $this->id . "/" . $this->photo;
    }

    public function one_fields()
    {

        $result = [
            'user' => [
                'id' => $this->id,
                'role_id' => $this->role_id,
                'role' => $this->role->name,
                'phone' => $this->phone,
                'email' => $this->email,
                'photo' => $this->photoPath,
                'first_name' => $this->first_name,
                'second_name' => $this->second_name,
                'last_name' => $this->last_name,
                'confirm' => $this->confirm,
                'balance' => $this->balance,
                'school_id' => $this->school_id,
                'school' => $this->school->name,
                'sity_id' => $this->sity_id,
                'sity_name' => $this->sity->name,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,

            ],
            'label' => $this->attributeLabels()
        ];
        if ($this->role_id == Role::LEARNER || $this->role_id == Role::TUTOR) {
            $result['user']['class'] = $this->myClass ? $this->myClass->number : null;
            $result['user']['letter'] = $this->myClass ? $this->myClass->letter : null;
        }
        if ($this->role_id != Role::LEARNER) {
            $result['my_classes'] = $this->myClasses;
        }
        if ($this->role_id == Role::TUTOR) {
            $my_pupil = new MyPupil();
            $all_pupols = $my_pupil->all_fields(['models' => $this->myPupils]);
            $result['pupils'] = $all_pupols['models'];
        }

        return $result;
    }


    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],

            [
                'common\models\User' => [
                    'id',
                    'role_id',
                    'role' => function ($model) {
                        return $model->role->name;
                    },
                    'phone',
                    'email',
                    'first_name',
                    'second_name',
                    'last_name',
                    'confirm',
                    'balance' => function ($model) {
                        if ($model->role_id == Role::LEARNER && $model->checkSubscription()) {
                            return $model->checkBalance();
                        }
                    },
                    'school_id',
                    'school' => function ($model) {
                        return $model->school->name;
                    },
                    'class' => function ($model) {
                        return $model->myClass;
                    },
                    'sity_id',
                    'sity_name' => function ($model) {
                        return $model->sity->name;
                    },
                    'discipline' => function ($model) {
                        return $model->myDisciplines;
                    },
                    'subscription' => function ($model) {
                        if ($model->role_id == Role::LEARNER && $model->checkSubscription()) {
                            return $model->subscription;
                        }
                    }
                ],
            ]
        );
        return $result;
    }

    public function all_fields_class($result)
    {
        $result = ArrayHelper::ToArray($result,
            [
                'common\models\User' => [
                    'id',
                    'role_id',
                    'role' => function ($model) {
                        return $model->role->name;
                    },
                    'phone',
                    'email',
                    'first_name',
                    'second_name',
                    'last_name',
                    'school_id',
                    'school' => function ($model) {
                        return $model->school->name;
                    },
                    'class' => function ($model) {
                        return $model->myClass;
                    },
                ],
            ]

        );
        return $result;
    }


    public function for_management_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
                [
                    'common\models\User' => [
                        'id',
                        'email',
                        'first_name',
                        'second_name',
                        'last_name',
                        'role_id',
                        'teacher' => function ($model) {
                            return $model->myTeachers;
                        },
                    ],
                ]

        );
        return $result;
    }

    /**
     * @return $this
     */
    public function saveModel()
    {
        if ($this->sity_name) {
            $sity = Sity::checkModel($this->sity_name, $this->region_name);
            if ($sity->getErrors()) {
                $this->addError('sity_name', $sity->getErrors());
                return $this;
            }
            $this->sity_id = $sity->id;
        }

        if ($this->isNewRecord) {
            $this->created_at = time();
        } else {
            $this->updated_at = time();
        }

        if ($this->save()) {

            if (($this->role_id == Role::LEARNER || $this->role_id == Role::TUTOR) && Yii::$app->request->post('class')) {
                $class = $this->myClass;
                if ($class == null) {
                    $class = new MyClass();
                }
                if(Yii::$app->request->post('letter')){
                    $class->letter = Yii::$app->request->post('letter');
                }
                if (!$class->saveOne($this->id, MyClass::TYPE_SELF, Yii::$app->request->post('class')) || $class->getErrors()) {
                    $this->addError('error', $class->getErrors());
                }
            }
            if ($this->role_id != Role::LEARNER) {
                $save_class = MyClass::checkModel($this->id, Yii::$app->request->post('my_classes'));
                if ($save_class && $save_class->getErrors()) {
                    $this->addError('error', $save_class->getErrors());
                }
            }

            if ($this->image_file) {
                 return $this->savePhoto();
            }
        }
        return $this;
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */

    public function signup()
    {
        $this->created_at = time();
        //сценарий для проверки полей формы
        $this->setScenario('signup');
        if ($this->validate()) {
            //сценарий для проверки сгенерированых полей
            $this->setScenario('save');
            $this->setPassword($this->password);
            $this->generateAuthKey();
            $this->saveModel();
        }
        return $this;
    }

    private function savePhoto()
    {
        $result = Yii::$app->uploadFile->uploadBase($this->image_file, $this->id, $this->extension);
        if (!$result) {
            return $this->addError('error', 'Image not saved');
        }
        if($this->photo){
            $old_photo = $this->photo;
        }
        $this->photo = $result;
        if($this->save() && $old_photo){
            $this->photo = $old_photo;
            unlink($this->photoDir);
            $this->photo = $result;
        }
        return $this;
    }


    public function checkSubscription()
    {

        $subs = Subscription::findOne(['subscription.user_id' => $this->id, 'subscription.deleted' => self::NOT_DELETED]);
        if ($subs) {
            if ($subs->date_to && $subs->date_to < time()) {
                $subs->remove();
            } else {
                return true;
            }
        }
        return false;
    }


    public function checkBalance()
    {
        $subs = $this->subscription;
        if ($subs) {
            $now = time();
            if ($now > $subs->date_to) {
                $day = $subs->date_to - $subs->date_from;
            } else {
                $day = $now - $subs->date_from;
            }

            $this->balance = $this->balance - intval($day / (3600 * 24));
            return $this->balance;
        }
        return false;
    }

    public function getDoneErrorTasks()
    {
        $res = Task::find()
            ->select(['task.id', 'task.text', 'task.explanation'])
            ->joinWith('doneTaskPush')
            ->where(['done_task.created_by' => $this->id])
            ->andOnCondition('task.result != done_task.answer')
            ->all();
        return $res;
    }

    public function getPushTask()
    {
        return Task::find()
            ->select(['task.id', 'task.text', 'task.explanation'])
            ->joinWith('doneTaskPush')
            ->where(['task.push_status' => 1])
            ->andOnCondition('(done_task.deleted = 1 AND done_task.created_by =' . $this->id . ' ) OR done_task.id is null')
            ->all();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoneTasks()
    {
        return $this->hasMany(DoneTask::className(), ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckups()
    {
        return $this->hasMany(Checkup::className(), ['created_by' => 'id'])->andOnCondition(['checkup.deleted' => self::NOT_DELETED]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getControls()
    {
        return $this->hasMany(Control::className(), ['created_by' => 'id'])->andOnCondition(['checkup.deleted' => self::NOT_DELETED]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyClasses()
    {
        return $this->hasMany(MyClass::className(), ['user_id' => 'id'])->andOnCondition(['type' => MyClass::TYPE_PUPIL, 'my_class.deleted' => self::NOT_DELETED]);
    }

    public function getMyClass()
    {
        return $this->hasOne(MyClass::className(), ['user_id' => 'id'])->andOnCondition(['type' => MyClass::TYPE_SELF, 'deleted' => self::NOT_DELETED]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyDisciplines()
    {
        return $this->hasMany(MyDiscipline::className(), ['user_id' => 'id'])->andOnCondition(['my_discipline.deleted' => self::NOT_DELETED]);
    }

    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['user_id' => 'id'])->andOnCondition(['subscription.deleted' => self::NOT_DELETED]);

    }


    public function getMyPupils()
    {
        if ($this->role_id == Role::CURATOR) {
            return $this->hasMany(MyPupil::className(), ['user_id' => 'id'])->andOnCondition(['my_pupil.type' => MyPupil::TUTOR_CURATOR, 'my_pupil.deleted' => self::NOT_DELETED]);
        } elseif ($this->role_id == Role::TUTOR) {
            return $this->hasMany(MyPupil::className(), ['user_id' => 'id'])->andOnCondition(['my_pupil.type' => MyPupil::PUPIL_TUTOR, 'my_pupil.deleted' => self::NOT_DELETED]);
        }
        return false;
    }

    public function getMyTeachers()
    {
        if ($this->role_id == Role::TUTOR) {
            return $this->hasMany(MyPupil::className(), ['pupil_id' => 'id'])->andOnCondition(['my_pupil.type' => MyPupil::TUTOR_CURATOR, 'my_pupil.deleted' => self::NOT_DELETED]);
        } elseif ($this->role_id == Role::LEARNER) {
            return $this->hasMany(MyPupil::className(), ['pupil_id' => 'id'])->andOnCondition(['my_pupil.type' => MyPupil::PUPIL_TUTOR, 'my_pupil.deleted' => self::NOT_DELETED]);
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransfers()
    {
        return $this->hasMany(Transfer::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(School::className(), ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSity()
    {
        return $this->hasOne(Sity::className(), ['id' => 'sity_id']);
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'deleted' => self::NOT_DELETED
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
//        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $phone
     * @return static|null
     */
    public static function findByPhone($phone)
    {
        return static::findOne([
            'phone' => $phone,
            'deleted' => self::NOT_DELETED
        ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'deleted' => self::NOT_DELETED,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getClasses()
    {
        return $this->myClasses;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


}
