<?php
namespace common\components;

use common\models\User;
use Yii;

class WebUser extends \yii\web\User
{
    private $_model = null;

    public function getRole(){
        if($user = $this->getModel()){
            return $user->role;
        }
    }

    private function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = User::findIdentity($this->id);
        }
        return $this->_model;
    }
}