/*
Navicat MySQL Data Transfer

Source Server         : home
Source Server Version : 50541
Source Host           : localhost:3306
Source Database       : math

Target Server Type    : MYSQL
Target Server Version : 50541
File Encoding         : 65001

Date: 2016-11-29 12:02:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `class` tinyint(1) NOT NULL,
  `edition` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `notes` text,
  `paid_content` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `book_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('5', 'математика', 'авттор', '5', 'бла', '2009', '11111111111', '0', '5', '19', '1476371373', '1478601850', '0');
INSERT INTO `book` VALUES ('7', 'англійська', 'автор', '5', 'ййй', '2000', 'фівфівфв', '0', '5', null, '1463713713', null, '0');
INSERT INTO `book` VALUES ('8', 'геометрія', 'ффф', '5', 'вап', '1875', 'івапр', '0', '5', '19', '1463714545', '1478264187', '0');
INSERT INTO `book` VALUES ('9', 'анг', 'авт', '5', 'выыф', '2008', 'ывам', '0', '19', null, '1478703343', null, '0');

-- ----------------------------
-- Table structure for bookmark
-- ----------------------------

-- ----------------------------
-- Table structure for translate
-- ----------------------------
DROP TABLE IF EXISTS `translate`;
CREATE TABLE `translate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `english_id` int(11) NOT NULL,
  `translate` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `english_id` (`english_id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `translate_ibfk_1` FOREIGN KEY (`english_id`) REFERENCES `eng_dictionary` (`id`),
  CONSTRAINT `translate_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `translate_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of translate
-- ----------------------------
INSERT INTO `translate` VALUES ('1', '1', 'кот', '5', '1476371373', null, null, '0');
INSERT INTO `translate` VALUES ('2', '2', 'словарь', '5', '1476371373', null, null, '0');
INSERT INTO `translate` VALUES ('3', '3', 'жизнь', '5', '1476371373', null, null, '0');
INSERT INTO `translate` VALUES ('4', '4', 'яблоко', '5', '1476371373', null, null, '0');
INSERT INTO `translate` VALUES ('5', '5', 'книга', '5', '1476371373', null, null, '0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) DEFAULT NULL,
  `sity_id` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `first_name` varchar(55) NOT NULL,
  `second_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(55) NOT NULL,
  `confirm` tinyint(1) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `balance` int(11) DEFAULT NULL,
  `push_status` tinyint(1) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `class` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `school_id` (`school_id`),
  KEY `sity_id` (`sity_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`),
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`sity_id`) REFERENCES `sity` (`id`),
  CONSTRAINT `user_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '2', '1', '1111111115', '1@1.c', '', '1', null, '1', null, '0ERxfRvpMjU_A8wWvXfFJcl3v5cfAU4i', '40', '0', '1478537751', '1471450288', '$2y$13$pTT6GdYCuHtzCf6bK0kgmemcN6LaqGOiCT.XEpaFu9W9.XyOdqh7W', '0', null);
INSERT INTO `user` VALUES ('2', '1', '3', '2', '1234567936', '', '45835a41.jpeg', '2', null, '233', null, 'ucqN16-chnGJGN0-a1y3IpBudtD__BOA', null, '0', '1479910420', '1471450630', '$2y$13$izR3bPT56MQjisvDBQ.RgevWmBOirwOYuI6kxuhT2UzEA0LoWQL9y', '0', null);
INSERT INTO `user` VALUES ('3', '1', '1', '3', '1234567936', '', null, '3', null, '3', null, 'pE6X5QJKcRPCSvHFh7PAaWSIiNsA90ZF', null, '0', '1471450693', '1471450693', '$2y$13$BIBWYUMYpXsCAqLoScds5equtwegb5u2oeiFxm6f/L5CcMbQjQZj.', '0', null);
INSERT INTO `user` VALUES ('4', '1', '2', '4', '1234567936', '4@4.c', null, '4', null, '4', null, 'VHDzWqTBGpdqlsbWrp9M0a618moki--C', '30', '0', '1474369779', '1471450733', '$2y$13$9R3.UnqY.Tizd2RRxLXs9O30Kux2IS8L.3DjCEe7RqIYUm6A5RxWe', '0', null);
INSERT INTO `user` VALUES ('5', '2', '3', '5', '1234567936', '', null, '5', null, '5', null, 'ZUCNeFIfLG52YaoOMCYn5Mt6ANNvUDNo', null, '0', '1471450830', '1471450830', '$2y$13$5ez6lDL8sspJKDNPc.1iYudXooQF.7R9xOh8s5PkKdNV1Pa8G2sau', '0', null);
INSERT INTO `user` VALUES ('6', '1', '4', '6', '1234567936', '', null, '6', null, '6', null, '0ERxfRvpMjU_A8wWvXfFJcl3v5cfAU45', null, '0', '1471450896', '1471450896', '$2y$13$gjioOYB2/f8QiSTsF/WQ/eogJxGeb5VKbYAxTXuHE5sKB/HM1LO6u', '0', null);
INSERT INTO `user` VALUES ('7', '1', '2', '1', '1234567808', 'tkalenko.inka@gmail.com', '7.jpg', 'www', null, 'www', null, 'gFyWAxTFjwvUG6AYiC1H_5hXCEfmZSw9', null, '0', '1471943148', '1471942913', '$2y$13$96ijlSxTuPaYlLUqGCpmI.TJ92lA3hnXjKK/58u2yG/tAOtSZDYQO', '0', null);
INSERT INTO `user` VALUES ('19', '1', '4', '5', '5555555555', 'manager@mail.com', null, 'manager', 'manager', 'manager', null, 'UXCxi-ULzubwkdRufzSwBJhz0WfbNcoN', null, '0', '1472813525', '1472813525', '$2y$13$oO6KgcoIBE45nA1kezvHPeUlPcgDWQ4.K2GV6jfBzqbjRt/y5ouNa', '0', null);
INSERT INTO `user` VALUES ('47', '1', '1', '4', '4444444444', 'curator@mail.com', '157d946b.png', 'Curator', 'Curator', 'Curator', null, 'juJehb6MHHI1ZbZdZK1_R-cXERDxABcn', null, '0', '1473857215', '1473845680', '$2y$13$nT2MwPhZrMTqcos2PVtOWOLtaJf0ej2T0fwZvyqtPAHyb.giOfX0q', '0', null);
INSERT INTO `user` VALUES ('50', '1', '1', '3', '3333333333', 'teacher@mail.com', '257da7d2.png', 'Teacher', 'Teacher', 'Teacher', null, 'jdrp4knkgRBoU4Ok9Ong_hQu7XW72pxJ', null, '0', '1473938715', '1473936672', '$2y$13$R2ZhRTuumxCsYi1nXTk7Aed5q1bm.CDT2MOGdUWVcwFsn0QSDQVve', '0', null);
INSERT INTO `user` VALUES ('52', '2', '1', '1', '1111111111', '', null, 'Сергей', 'Охрименко', '12345454', null, 'rDetzRZA-e5h-XnWO4Aa73mveXXU4Y8V', null, '0', '1477579612', '1477579612', '$2y$13$v1hYBfI1PsxzyLgoe3AS0uO/f1xuNwbZTw6v9hLf0xfnsDVYMk69i', '0', null);
INSERT INTO `user` VALUES ('95', '3', '1', '2', '6210693167', '', null, 'User', 'User', '12345454', null, 'mu7szkuYFbqsttlPbms1w3g1JdxwZ65c', null, '0', '1478612761', '1478612761', '$2y$13$6oyhbk1aMgOQf581Rtx4VOzvP/Hi4wnDCJE1veKMcou0P12BDf/Dy', '0', null);
INSERT INTO `user` VALUES ('97', '3', '1', '2', '7004159492', '', null, 'User', 'User', '12345454', null, '6pUsRSWybRHIvHY4JhbErqY048N7IYUr', null, '0', '1478612804', '1478612804', '$2y$13$g6aZU8cycXaaMOzbU/hZ1eMfyHd0bX.QY9oozLTJRW4DjgtchWOr2', '0', null);
INSERT INTO `user` VALUES ('99', '3', '2', '2', '4644980062', '', null, 'User', 'User', '12345454', null, 'e3zVZ9y25AocHGppaysozNSg7mWgt7pk', null, '0', '1478612878', '1478612878', '$2y$13$/aHY0ACa1kDUoqU8tAtrSeHfesrMRK83juSbYOBSjcEfb8k2ECoXi', '0', null);
INSERT INTO `user` VALUES ('100', '3', '1', '2', '5796789571', '', null, 'User', 'User', '12345454', null, '1UcMviWBpWnuBlERPnNrrmKklVP7KSBX', null, '0', '1478612920', '1478612920', '$2y$13$7OwpLp31Pi2LV0lh46Y1Neq8f/hOpI2X7Gqp32jEBTz50V.ul3BnS', '0', null);
INSERT INTO `user` VALUES ('101', '3', '1', '2', '7659078371', '', null, 'User', 'User', '12345454', null, 'hXk-1PMsncjOn2iGceij7PthM_ncR33z', null, '0', '1478612988', '1478612988', '$2y$13$CV8kDJ7DGseZlXUpOhqmEerJV6NAzmBcPUGet6WOW486mvZCnrJ4K', '0', null);
INSERT INTO `user` VALUES ('102', '3', '1', '2', '9008365524', '', null, 'User', 'User', '12345454', null, 'VsmNSUdaaxwVt4mzupXKfs-fPqHbHyAi', null, '0', '1478612990', '1478612990', '$2y$13$fGWPYGrwnumfiM.ySSMsbe1wa7zsBgaTxF0/6YAT5IkP4/s3aDct6', '0', null);
INSERT INTO `user` VALUES ('103', '3', '2', '2', '2018355223', '', null, 'User', 'User', '12345454', null, 'SKKhypsTFGys4i_urcwBcAe4_2pUuAcL', null, '0', '1478612992', '1478612992', '$2y$13$rRYXvrmPS3MiTgts6GXQ4eqzLzyUn2Jrk.GL7m4.CGv3fhIXQudMS', '0', null);
INSERT INTO `user` VALUES ('104', '3', '1', '2', '7463201588', '', null, 'User', 'User', '12345454', null, 'y0Xg6YCx70c5mh6vIiFEFPv3ZM27Hd6B', null, '0', '1478612994', '1478612994', '$2y$13$iZ60bE/6JqSGiuvYED3f9edpSO.irgQt4S0sAdZtJ4fAx7gGhik6u', '0', null);
INSERT INTO `user` VALUES ('105', '3', '1', '2', '7761672861', '', null, 'User', 'User', '12345454', null, 'QF2MnTRqqXGOZpULXxlVGEP5b3H7BnvP', null, '0', '1478612997', '1478612997', '$2y$13$pA1rD7k1C2vhJ8hqh4YPhuoAqf7yBOAmpKIM3NjZVDwnIWXzcdJjq', '0', null);
INSERT INTO `user` VALUES ('106', '3', '4', '2', '8116811766', '', null, 'User', 'User', '12345454', null, 'V6D7YBoTUCjUWgyOl_NmyD11Qz-AKDUG', null, '0', '1478612999', '1478612999', '$2y$13$uFlxkPXvlxozk93peNi9bOGHGssfB5GrlRaWe7yPnQ6.foN5yI/wG', '0', null);
INSERT INTO `user` VALUES ('107', '3', '1', '2', '1388619875', '', null, 'User', 'User', '12345454', null, 'kqFgdGR-x-SCwTYWY9ZxyfHiRsFMVfki', null, '0', '1478613001', '1478613001', '$2y$13$00Es8cTGFInSlpcPzjWzIOTaPG6vsFjiRB/c2.oBUNo5ekE6n9eB.', '0', null);
INSERT INTO `user` VALUES ('108', '3', '1', '2', '2378586379', '', null, 'User', 'User', '12345454', null, 'jjc9DxJYY8VK8KsIMhap2rMuqjIwcDV6', null, '0', '1478613003', '1478613003', '$2y$13$Ot.E0Ehj0HZJnC7yRK5.suF6phNyFkY.7xX/a/jsq7x7doLjWuEXy', '0', null);
INSERT INTO `user` VALUES ('109', '3', '1', '2', '1904394297', '', null, 'User', 'User', '12345454', null, 'WyuqXiHNuGz2I62s02QFDF4O5zMJ-4du', null, '0', '1478613006', '1478613006', '$2y$13$YjNF.6mJ8dZO/Folojl6gubnP7.mRAdWoYHotLDOfq5EvtP.ukbkS', '0', null);
INSERT INTO `user` VALUES ('110', '3', '1', '2', '4262921001', '', null, 'User', 'User', '12345454', null, 'mbwVdO8XNm2T8UE_VQmhW_--lwuI99Ao', null, '0', '1478613008', '1478613008', '$2y$13$CziqyeBD.4f/jXJKDGJ6kumEbcukghXKfEKw6FJnxdtYur9Oa6Vbu', '0', null);
INSERT INTO `user` VALUES ('111', '3', '1', '2', '4229195848', '', '', 'User', 'User', '12345454', null, 'zk8yoYl9enHeYG0hJNq9Dm5sPdlar3Hq', null, '0', '1478709733', '1478705271', '$2y$13$Qzj7sKBuDzjSh1el6EI2SuSuvKCv05zpxnYiHe6qvlfy9pyT2nA1K', '0', null);
INSERT INTO `user` VALUES ('115', '4', '1', '1', '661111111', '', null, 'ацуауц', null, 'ауауц', null, 'EFkHFkSf8mvVaWkRWEsvuFRlGL1uMJI3', null, '0', '1479814686', '1479814686', '$2y$13$wHOH0Ji7UXvsQnUaZktzCexwAidBPEUFnLSUB6KoYDL91rVggM/Lu', '0', null);
INSERT INTO `user` VALUES ('116', '2', '1', '1', '0951111111', '', null, '212121', null, '22121', null, '5o_WU7JtrvzACSErlf2_oJR_jzfU4t-z', null, '0', '1479814875', '1479814875', '$2y$13$ukb/BZypFSYYgrdGeZ3I0OofXnUiuDycrRptBgRvaqUKPeMLKdFOq', '0', null);
INSERT INTO `user` VALUES ('117', '5', '1', '1', '0202020202', '', null, 'пукпукпук', null, 'ааукпук', null, '8XUySfhxTEbdSj4H-MiX-wgDsNcxK5Os', null, '0', '1479815379', '1479815379', '$2y$13$IHOj4Cf56INU6AAahE/m5.RQOsljl7EC7kYeZfZo6zBATH2pB308q', '0', null);
INSERT INTO `user` VALUES ('119', '5', '1', '1', '0303300303', '', null, 'екркерке', null, 'рекркер', null, 'Y5ZvTOCeFIkz-Fn1gWMR3lP4z5oFv58U', null, '0', '1479817470', '1479815553', '$2y$13$CnLj9OBLfHSE9v9R0mOdMeoWvFwN8C9rI1.AVU.nA/d2R1K1gfur2', '0', null);
INSERT INTO `user` VALUES ('120', '2', '1', '2', '5454654646', '', null, 'r43t43t43', null, '312321f', null, 'SiIaWUpklmsFXmU2v9lWtJhf2SBV6dcB', null, '0', '1479825041', '1479825041', '$2y$13$nGFxuDso7XYlBxuMXCZTLOd//wTRQEfMEbmXQFi1f8E8u57K1FV96', '0', null);
INSERT INTO `user` VALUES ('121', '4', '1', '2', '4343243423', '', '65834580.jpeg', 'gregerg', null, 'gerger', null, 'u2HZ23OTLy_B7p9vHhyrvgcmNYRXA-VA', null, '0', '1479825413', '1479825239', '$2y$13$ACwV3tltgQIORWqFlx1EeOYExIhXPJt/NqMsZrYou0QwAH7DCkxp6', '0', null);

-- ----------------------------
-- Table structure for user_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `user_dictionary`;
CREATE TABLE `user_dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `translate_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `deleted` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `translate_id` (`translate_id`),
  CONSTRAINT `user_dictionary_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_dictionary_ibfk_2` FOREIGN KEY (`translate_id`) REFERENCES `translate` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_dictionary
-- ----------------------------
INSERT INTO `user_dictionary` VALUES ('1', '1', '1', '0', '1');
INSERT INTO `user_dictionary` VALUES ('2', '1', '2', '0', '1');
INSERT INTO `user_dictionary` VALUES ('4', '1', '3', '1478099527', '1');
INSERT INTO `user_dictionary` VALUES ('5', '1', '4', '1478104607', '1');
INSERT INTO `user_dictionary` VALUES ('6', '1', '5', '1478188028', '1');

DROP TABLE IF EXISTS `eng_dictionary`;
CREATE TABLE `eng_dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `explanation` varchar(255) DEFAULT NULL,
  `intensity` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `eng_dictionary_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  CONSTRAINT `eng_dictionary_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of eng_dictionary
-- ----------------------------
INSERT INTO `eng_dictionary` VALUES ('1', 'cat', null, '0', '5', '1476371373', null, null, '0');
INSERT INTO `eng_dictionary` VALUES ('2', 'dictionary', null, '0', '5', '1476371373', null, null, '0');
INSERT INTO `eng_dictionary` VALUES ('3', 'life', null, '0', '5', '1476371373', null, null, '0');
INSERT INTO `eng_dictionary` VALUES ('4', 'apple', null, '0', '5', '1476371373', null, null, '0');
INSERT INTO `eng_dictionary` VALUES ('5', 'book', null, '0', '5', '1476371373', null, null, '0');
